Create a kernel for iPython to run.

poetry run ipython kernel install --user --name=<NAME_OF_KERNEL>

<NAME_OF_KERNEL> can be anything but should be the name of the project.
