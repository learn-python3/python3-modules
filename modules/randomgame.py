import sys
import random

if len(sys.argv) != 3:
    sys.exit("Please run the file with `randomgame.py minValue maxValue")


_, minValue, maxValue = sys.argv

try:
    number = random.randint(int(minValue), int(maxValue))
except:
    sys.exit("`minValue` and `maxValue` must be integers! Exit.")

userInput = None

while userInput != number:
    try:
        userInput = int(input("Guess the integer: "))
    except:
        print(
            f"Not an integer. Please use only integers between {minValue} and {maxValue}."
        )
        continue
    finally:
        print("Try again")

else:
    print(f"Correct: the number was {number}. You win.")
